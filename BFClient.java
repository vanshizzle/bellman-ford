import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Bread and butter class for this programming assignment.
 * Includes logic for creating ourselves as BF clients as well as reading in the file.
 * @author Vanshil
 *
 */
public class BFClient {
	
	private int timeout;
	private int myPort;
	private String chunkFile;
	private RoutingTable routingTable;
	private boolean fileSendingClient;
	private FileTransferMessage fileMessage;
	
	private Map<String, TimerTask> timeouts;
	private Map<String, DistanceVector> timedoutClients;
	private Timer timeoutTimer;
	
	//Networking variables
	private DatagramSocket readSock;
	private DatagramSocket writeSock;
	private DatagramSocket fileRcvSock;
	private ByteArrayOutputStream bos;
	private ByteArrayInputStream bis;
	private ObjectOutputStream outStream;
	private ObjectInputStream inStream;
	private String myIP;
	private byte inputBuffer[];
	private byte fileBuffer[];
	private byte chunk1Buffer[];
	private byte chunk2Buffer[];
	private byte concatFileBuffer[];
	private boolean chunksReceived[];
	
	private final int BUFFERSIZE = 2048;
	private final int MAXBUFFERSIZE = 65000;
	private final int MESSAGE_TYPES[] = {
			0,	//Route update
			1,	//link down
			2,	//link up
			3,	//Transfer request
			4	//Transfer ACK
	};
	
	public BFClient(int myPort, int timeout) {
		this.myPort = myPort;
		this.timeout = timeout;
		routingTable = new RoutingTable(myPort);
		try {
			myIP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		inputBuffer = new byte[BUFFERSIZE];
		timeouts = new HashMap<String, TimerTask>();
		timeoutTimer = new Timer();
		timedoutClients = new Hashtable<String, DistanceVector>();
		chunksReceived = new boolean[2];
	}
	
	public void addChunkFile(String fileName) {
		chunkFile = fileName;
	}

	public void setFileSendingClient(boolean fileSendingClient) {
		this.fileSendingClient = fileSendingClient;
	}
	
	public void initReadSock() {
		try {
			readSock = new DatagramSocket(myPort);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void closeReadSock() {
		readSock.close();
	}
	
	public void addDistanceVector(String IP, int port, double weight, String nextHopIP, int nextHopPort) {
		//We only use this initially to set up the clients from the config file so 
		//lets make them all our neighbors, specified by the last true
		routingTable.addDv(IP, port, weight, nextHopIP, nextHopPort, true);
	}
	
	public void printRoutingTable() {
		System.out.println(new Date() + "     Distance Vector List is");
		routingTable.printMap();
	}
	
	/**
	 * Sends an individual udp message to the specified IP and port
	 * @param o the object to be send
	 * @param IP	ip to send to
	 * @param port	port to send to
	 */
	public void sendUDPMessage(Object o, String IP, int port) {
		try {
			writeSock = new DatagramSocket();
			bos = new ByteArrayOutputStream(MAXBUFFERSIZE);
			outStream = new ObjectOutputStream(bos);
			outStream.writeObject(o);
			DatagramPacket sendPacket = new DatagramPacket(
													bos.toByteArray(), 
													bos.size(), 
													InetAddress.getByName(IP),
													port);
			if(!writeSock.isClosed()) {
				writeSock.send(sendPacket);
			}
			writeSock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			
			//Lets just silently break
		}
	}
	
	/**
	 * Send our routing table to all of our lovely neighbors
	 * 
	 */
	public synchronized void sendTable() {
		Map<String, DistanceVector> map = routingTable.getMap();
		for(String s : map.keySet()) {
			DistanceVector dv = map.get(s);
			//Don't send a distance vector if this person is not a neighbor 
			// OR the cost to them is too expensive
			if(!dv.isNeighbor() || dv.getCost()==Double.POSITIVE_INFINITY) {
				continue;
			}
			String temp[] = s.split(":");
			String IP = temp[0];
			int port = Integer.parseInt(temp[1]);
			
			//System.out.println("Sending to: " + IP + ":" + port + " with cost " + dv.getCost());
			
			//Create the message we're going to be sending
			NetworkMessage sm = new NetworkMessage(MESSAGE_TYPES[0]);
			RoutingTable toSend = poisonReverseRT(IP, port);
			sm.setRT(toSend);
			sendUDPMessage(sm, IP, port);
		}
	}
	
	/**
	 * Poison reverse the routing table we're going to send.
	 *
	 * @param IP
	 * @param port
	 * @return
	 */
	public RoutingTable poisonReverseRT(String IP, int port) {
		RoutingTable copy = routingTable.copy();	//Create a new copy of our routing table
		Map<String, DistanceVector> map = copy.getMap();	
		for(String s : map.keySet()) {
			DistanceVector dv = map.get(s);
			
			/* Essence of poison reversing:
			 * If the person we're trying to send to is our next hop to get to a different host,
			 * then we advertise that our distance to that host is actually infinity.
			 * Also added that the next hop should actually just be myself.
			 */
			if(dv.nextHop().getIP().equals(IP) && dv.nextHop().getPort() == port) {
				dv.setCost(Double.POSITIVE_INFINITY);
				dv.setHop(myIP, myPort);
			}
		}
		return copy;
	}
	
	
	/**
	 * Called when we receive a route update message from a different client
	 * @param fromIP
	 * @param fromPort
	 * @param received
	 * @throws UnknownHostException
	 */
	public synchronized void updateRoutingTable(String fromIP, int fromPort, RoutingTable received) throws UnknownHostException {		
		Map<String, DistanceVector> theirMap = received.getMap();
		Map<String, DistanceVector> ourMap = routingTable.getMap();
		double fromCost = ourMap.get(fromIP + ":" + fromPort).getCost();
		
		/*
		 * Going through their distance vector to update our costs.
		 */
		for(String s : theirMap.keySet()) {
			String temp[] = s.split(":");
			String IP = temp[0];
			int port = Integer.parseInt(temp[1]);
			double cost = theirMap.get(s).getCost();
			
			//System.out.println(theirMap.get(s).toString());
			
			DistanceVector dv = ourMap.get(s);
			
			/* This person doesn't exist in our routing table
			 * AND 
			 * this person isn't us because we don't hold ourselves in our own routing table
			 */
			if(dv==null && !((IP.equals("127.0.0.1") || IP.equals(myIP)) && (port==myPort))) {	
				routingTable.addDv(IP, port, cost+fromCost, fromIP, fromPort, false);
				routingTable.markDirty();
				System.out.println("Added: " + IP + ":" + port + " through " + fromIP + ":" + fromPort);
			} else if(dv!=null && (timedoutClients.get(IP + ":" + port) == null)) {
				//System.out.println("Rec DV: " + theirMap.get(s));
				//We've received a packet from someone who's link went down, lets make sure not to use that link
				if(cost==Double.POSITIVE_INFINITY) {
					if(dv.nextHop().equals(fromIP + ":" + fromPort)) {
						
						DistanceVector orig = routingTable.getOriginalMap().get(s);
						
						if(orig!=null) {
							dv.setCost(orig.getCost());
							//System.out.println("Original vector: " + orig.toString());
							dv.setHop(myIP, myPort);
							//System.out.println("Update Distance Vector 1: " + dv.toString());
						} else {
							dv.setCost(Double.POSITIVE_INFINITY);
							dv.setHop(myIP, myPort);
							//System.out.println("Update Distance Vector 2: " + dv.toString());
						}
					}
					continue;
				}
				if(cost + fromCost < dv.getCost()) {
					
					//If we go through someone to get to this guy then set our next hop to that guy.
					DistanceVector toThem = ourMap.get(fromIP + ":" + fromPort);
					if(!toThem.nextHop().equals(myIP + ":" + myPort)) {
						dv.setHop(toThem.nextHop().getIP(), toThem.nextHop().getPort());
					} else {
						dv.setHop(fromIP, fromPort);
					}
					dv.setCost(cost+fromCost);
					routingTable.markDirty();
					System.out.println("Updated: " + IP + ":" + port + " through " + fromIP + ":" + fromPort);
				}
			}
		}
	}
	
	/**
	 * Called when we receive a linkup/linkdown message
	 * 
	 * If we're going through the person to whom the link was broken, then we change those costs to infinity
	 */
	public synchronized void reroute(String fromIP, int port) {
		Map<String, DistanceVector> map = routingTable.getMap();
		Map<String, DistanceVector> origMap = routingTable.getOriginalMap();
		DistanceVector origDV;
		
		DistanceVector dv = routingTable.getMap().get(fromIP + ":" + port);
		dv.setCost(Double.POSITIVE_INFINITY);
		dv.setHop(myIP, myPort);
		
		for(String s : map.keySet()) {
			dv = map.get(s);
			origDV = origMap.get(s);
			/*
			 * The person who just told us to bring a link down is our next hop, so 
			 * lets set distance to them to infinity for the time being until the network gets
			 * resolved.
			 * 
			 * Also have to check that if we have a direct link to them then we should restore 
			 * that direct link.
			 * 
			 */
			if(dv.nextHop().equals(fromIP + ":" + port)) {
				if(origDV==null) {
					dv.setCost(Double.POSITIVE_INFINITY);
					dv.setHop(myIP, myPort);
					//System.out.println("Reroute Distance Vector 1: " + dv.toString());
				} else if(origDV!=null) {
					dv.setCost(origDV.getCost());
					dv.setHop(myIP, myPort);
					//System.out.println("Reroute Distance Vector 2: " + dv.toString());
				}
			}
			routingTable.markDirty();
		}
	}
	
	
	public int linkDown(String IP, int port) {
		int response = 0;
		Map<String, DistanceVector> map = routingTable.getMap();
		Map<String, DistanceVector> origMap = routingTable.getOriginalMap();
		DistanceVector dv = map.get(IP + ":" + port);
		
		/* We only want to take this link down if the address we're given is our neighbor
		 * AND we haven't already destroyed that link
		 */
		if(dv!=null && dv.isNeighbor() && dv.getCost()!=Double.POSITIVE_INFINITY) {
			dv.setCost(Double.POSITIVE_INFINITY);
			dv.setHop(myIP, myPort);
			
			//Just making sure to set everyone who we get to through the link is also 
			//marked 
			
			for(String s: map.keySet()) {
				DistanceVector vector = map.get(s);
				if(vector.nextHop().equals(IP + ":" + port)) {
					//If this person is in our original neighbor map, we'll just restore the original link
					if(origMap.get(s) != null) {
						vector.setCost(origMap.get(s).getCost());
						vector.setHop(myIP, myPort);
					} else {
						vector.setCost(Double.POSITIVE_INFINITY);
					}
				}
			}
			
			routingTable.markDirty();
			//Create the message we'll be sending
			NetworkMessage sm = new NetworkMessage(MESSAGE_TYPES[1]);
			sm.setPort(myPort);
			sendUDPMessage(sm, IP, port);
		} else {
			response = -1;
		}
		return response;
	}
	
	
	public int linkUP(String IP, int port, double cost) {
		int response = 0;
		DistanceVector dv = routingTable.getMap().get(IP + ":" + port);
		if(dv!=null && dv.isNeighbor()) {
			dv.setCost(cost);
			dv.setHop(myIP, myPort);
			routingTable.markDirty();
			
			NetworkMessage sm = new NetworkMessage(MESSAGE_TYPES[2]);
			sm.setCost(cost);
			sm.setPort(myPort);
			sendUDPMessage(sm, IP, port);
		} else {
			response = -1;
		}
		
		return response;
	}
	
	/**
	 * Called when a client who timed out comes back into the network.
	 * @param IP
	 * @param port
	 * @param rt
	 */
	public void reopen(String IP, int port, RoutingTable rt) {
		Map<String, DistanceVector> theirMap = rt.getMap();
		
		DistanceVector dvToUs = theirMap.get(myIP + ":" + myPort);
		if(dvToUs != null) {
			DistanceVector dvToThem = routingTable.getMap().get(IP + ":" + port);
			
			//Set our cost to them equal to their cost to us.
			dvToThem.setCost(dvToUs.getCost());	
			
			timedoutClients.remove(IP + ":" + port);
			
			routingTable.markDirty();
		}
	}
	
	
	public void transferConfig(String IP, int port) {
		//First lets deal with setting up the receiver for the file transfer
		//Send transfer request
		NetworkMessage n = new NetworkMessage(MESSAGE_TYPES[3]);
		n.setPort(myPort);
		sendUDPMessage(n, IP, port);
	}
	
	public void readFile() throws IOException {
		//Have to read the file into the filetransfermessage here
		File file = new File(chunkFile);
		byte[] fileData = new byte[(int) file.length()];
		DataInputStream dis = new DataInputStream(new FileInputStream(file));
		dis.readFully(fileData);
	    dis.close();
		fileMessage.setData(fileData);
	}
	
	private class FileReceiverThread implements Runnable {
		public synchronized void run() {
			//Start receiving the file here
			fileBuffer = new byte[MAXBUFFERSIZE];
			DatagramPacket incomingPacket = new DatagramPacket(fileBuffer,
														fileBuffer.length);
			try {
				fileRcvSock.receive(incomingPacket);
				byte[] data = incomingPacket.getData();
				ByteArrayInputStream input = new ByteArrayInputStream(data);
				ObjectInputStream stream = new ObjectInputStream(input);
				//Read the send message type object
				FileTransferMessage message = (FileTransferMessage) stream.readObject();
				System.out.println("Received file sent at: " + message.timestamp);
				
				if(!(message.destinationIP.equals(myIP) && message.destinationPort==myPort)) {
					//This isn't meant for us. Lets just forward it.
					DistanceVector dv = routingTable.getMap().get(message.destinationIP + ":" + message.destinationPort);
					
					if(dv==null) {
						//Idk why we received this packet
					} else {
						//Lets forward it
						String nextHopIP = dv.nextHop().getIP();
						int nextHopPort = dv.nextHop().getPort();
						
						String destIP = dv.getDestination().getIP();
						int destPort = dv.getDestination().getPort();

						fileMessage = message;
						
						//This person isn't our neighbor so we send to the next hop
						if(!(nextHopIP.equals(myIP) && nextHopPort==myPort)) {
							transferConfig(nextHopIP, nextHopPort);	//Thats who we have to send to.
							fileMessage.setNextHop(nextHopIP, nextHopPort);
						} else {
							transferConfig(destIP, destPort);
							fileMessage.setNextHop(destIP, destPort);
						}
					}
				} else if(message.destinationIP.equals(myIP) && message.destinationPort==myPort) {
					//End logic
					System.out.println("From: " + message.sourceIP + ":" + message.sourcePort);
					System.out.println("Path Traversed: ");
					for(String s : message.hops) {
						if(!(s==null))
							System.out.println("\t" + s);
					}
					System.out.println("\nTime Sent: " + message.timestamp);
					System.out.println("Time arrived: " + new Date());
					System.out.println("Size: " + message.data.length + " bytes");
					System.out.println("Chunk: " + message.fileSequenceNumber);

					if(message.fileSequenceNumber==1) {
						chunk1Buffer = new byte[message.data.length];
						System.arraycopy(message.data, 0, chunk1Buffer, 0, message.data.length);
						chunksReceived[0] = true;
					} else if(message.fileSequenceNumber==2) {
						chunk2Buffer = new byte[message.data.length];
						System.arraycopy(message.data, 0, chunk2Buffer, 0, message.data.length);
						chunksReceived[1] = true;
					}
					
					//Both are received
					if(chunksReceived[0] && chunksReceived[1]) {
						System.out.println("Both chunks received......Writing to file");
						File file = new File("output");
						DataOutputStream d = new DataOutputStream(new FileOutputStream(file));
						concatFileBuffer = new byte[chunk1Buffer.length + chunk2Buffer.length];
						ByteBuffer target = ByteBuffer.wrap(concatFileBuffer);
						target.put(chunk1Buffer);
						target.put(chunk2Buffer);
						System.out.println("Started writing to the file.");
						d.write(concatFileBuffer);
						d.close();
						System.out.println("Finished writing to the file.");
					} else {
						//Starts the writing thread as soon as it receives one of the chunks
						//We don't really need to do anything
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private class FileSenderThread implements Runnable {
		int txPort;
		
		public FileSenderThread(int port) {
			txPort = port;
		}
		
		public void run() {
			//Its going to wait 1 second to make sure the file is ready to be transferred
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sendUDPMessage(fileMessage, fileMessage.nextHopIP, txPort);
		}
	}
	
	
	/**
	 * This is the receiver thread class
	 * @author Vanshil
	 *
	 */
	private class Receiver implements Runnable {
		
		/**
		 * The method that first gets called 
		 */
		public void run() {
			try {
				receiveData();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public synchronized void receiveData() throws ClassNotFoundException{
			while(!Thread.currentThread().isInterrupted()) {
				try {
					DatagramPacket incomingPacket = new DatagramPacket(inputBuffer,
															inputBuffer.length);
					readSock.receive(incomingPacket);
					byte[] data = incomingPacket.getData();
					bis = new ByteArrayInputStream(data);
					inStream = new ObjectInputStream(bis);
					
					//Read the sendmessage type object
					NetworkMessage message = (NetworkMessage) inStream.readObject();
					
					String fromIP = incomingPacket.getAddress().toString().replaceFirst("/", "");
					if(message.messageType == 0) {
						//We're dealing with a ROUTE UPDATE message
						
						RoutingTable rt = message.rt;
				       	int fromPort = rt.getMyPort();
				       	TimerTask t = timeouts.get(fromIP + ":" + fromPort);
				       	if(t!=null) {
				       		t.cancel();
				       		t = new TimeoutChecker(fromIP + ":" + fromPort);
				       		timeoutTimer.schedule(t, timeout*1000*3);
				       		timeouts.put(fromIP + ":" + fromPort, t);
				       	} else {
				       		//We closed this client previously
				       		t = new TimeoutChecker(fromIP + ":" + fromPort);
				       		timeoutTimer.schedule(t, timeout*1000*3);
				       		timeouts.put(fromIP + ":" + fromPort, t);
				       		reopen(fromIP, fromPort, rt);
				       	}
				       	//System.out.println("Routing table received from " + fromIP + ":" + fromPort);
				       	updateRoutingTable(fromIP, fromPort,  rt);
				       	
					} else if(message.messageType == 1) {
						//We're dealing with a LINK DOWN message
						
						int port = message.fromPort;
						
						//We have to reset our timer now
						TimerTask t = timeouts.get(fromIP + ":" + port);
				       	if(t!=null) {
				       		t.cancel();
				       		t = new TimeoutChecker(fromIP + ":" + port);
				       		timeoutTimer.schedule(t, timeout*1000*3);
				       		timeouts.put(fromIP + ":" + port , t);
				       	}
						
						//System.out.println("LINK DOWN received from: " + fromIP + ":" + port);
						
						reroute(fromIP, port);
						
					} else if(message.messageType == 2) {
						//We're dealing with a LINK UP message
						
						int port = message.fromPort;
						TimerTask t = timeouts.get(fromIP + ":" + port);
				       	if(t!=null) {
				       		t.cancel();
				       		t = new TimeoutChecker(fromIP + ":" + port);
				       		timeoutTimer.schedule(t, timeout*1000*3);
				       		timeouts.put(fromIP + ":" + port, t);
				       	}
						
						
						double cost = message.cost;
						
						DistanceVector dv = routingTable.getMap().get(fromIP + ":" + port);
						dv.setCost(cost);
						dv.setHop(myIP, myPort);
						routingTable.markDirty();
					} else if(message.messageType == 3) {
						//We're dealing with a TRANSFER REQUEST 
						
						int port = message.fromPort;
						System.out.println("TX request from: " + fromIP + ":" + port);
						
						fileRcvSock = new DatagramSocket();
						
						NetworkMessage n = new NetworkMessage(MESSAGE_TYPES[4]);
						n.setTxPort(fileRcvSock.getLocalPort());
						n.setPort(myPort);
						sendUDPMessage(n, fromIP, port);
						
						//Start the file receiver thread
						Thread t = new Thread(new FileReceiverThread());
						t.start();
						
					} else if(message.messageType == 4) {
						//We're dealing with a TRANSFER ACK
						
						int port = message.fromPort;
						System.out.println("TX ack from: " + fromIP + ":" + port);
						
						Thread t = new Thread(new FileSenderThread(message.txPort));
						t.start();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
					//Just want to silently break
					break;
				} 
			}
			System.out.println("Receiver thread finished.");
		}
	}
	
	
	/**
	 * Thread dedicated to sending route update messages to our neighbors
	 * ITS VERY INELEGANT! SORRY
	 * @author vanshil
	 *
	 */
	private class Updater implements Runnable {
		private Date lastSent;
		private Date current;
		public void run() {
			//Check for updates in our Routing table
			checkForUpdates();
		}
		public void checkForUpdates() {
			lastSent = new Date();
			while(!Thread.currentThread().isInterrupted()) {
				current = new Date();
				boolean timeForUpdate = (current.getTime() - lastSent.getTime()) >= timeout*1000;
				if(routingTable.hasChanged() || timeForUpdate) {
					sendTable();
					routingTable.markClean();
					lastSent = new Date();
				}
			}
			System.out.println("Update Thread finished.");
		}
	}
	
	/**
	 * An instance of this class exists with every one of our neighbors in a map
	 * so that we can keep track of how many clients we have and when they all 
	 * time out.
	 * @author vanshil
	 *
	 */
	private class TimeoutChecker extends TimerTask {
		private String client;
		public TimeoutChecker(String client){
			this.client = client;
		}
		
		public synchronized void run() {
			//If we get here that means we timed out
			//Do some useful shit
			/*
			 * Tasks:
			 * - 
			 */
			System.out.println(client + " timed out");
//			DistanceVector dv = routingTable.getMap().get(client);
//			dv.setCost(Double.POSITIVE_INFINITY);
//			dv.setHop(myIP, myPort);
//			routingTable.markDirty();
			String split[] = client.split(":");
			timeouts.put(client, null);
			timedoutClients.put(client, routingTable.getMap().get(client));
			linkDown(split[0], Integer.parseInt(split[1]));
		}
	}
	
	/**
	 * The main method for this client
	 * @param args
	 * @throws FileNotFoundException
	 * @throws UnknownHostException 
	 * @throws SocketException 
	 */
	public static void main(String[] args) throws FileNotFoundException, UnknownHostException {
		
		FileReader fr = new FileReader(args[0]);
		
		Scanner s = new Scanner(new BufferedReader(fr));
		Scanner actions = new Scanner(System.in);
		BFClient me;
		
		int myPort;
		int timeout;
		int fileSeqNum = -1;
		String chunkFile = null;
		
		//The following help with the reading of the file
		String tempIP;
		int tempPort;
		double tempCost;
		
		String line, splitLine[];
		splitLine = new String[4];
		if(!s.hasNext()) {
			System.out.println("Empty File.");
			return;
		}
		
		//Parse in this client's information
		line = s.nextLine();
		splitLine = line.split(" ");
		myPort = Integer.parseInt(splitLine[0]);
		timeout = Integer.parseInt(splitLine[1]);
		try {
			chunkFile = splitLine[2];
			fileSeqNum = Integer.parseInt(splitLine[3]);
		} catch (IndexOutOfBoundsException i) {
			//Doesn't really matter. Just means that we don't have those fields
			//in our config file.
		}
		
		//Create this client's instance
		me = new BFClient(myPort, timeout);
		if(chunkFile != null && fileSeqNum != -1) {
			me.addChunkFile(chunkFile);
			me.setFileSendingClient(true);
		}
		
		//Parses the rest of the clients
		while(s.hasNext()) {
			line = s.nextLine();
			splitLine = line.split(":", 2);
			tempIP = splitLine[0];
			splitLine = splitLine[1].split(" ", 2);
			tempPort = Integer.parseInt(splitLine[0]);
			tempCost = Double.parseDouble(splitLine[1]);	
			me.addDistanceVector(tempIP, tempPort, tempCost, me.myIP, myPort);
			TimerTask t = me.new TimeoutChecker(tempIP + ":" + tempPort);
			me.timeoutTimer.schedule(t, timeout*3*1000);
			me.timeouts.put(tempIP + ":" + tempPort, t);
		}

		//Set up our read sockets
		me.initReadSock();
		
		System.out.println("My IP: " + me.myIP + ", My Port: " + me.myPort);
		System.out.println("Please enter a command. ");
		
		Thread rec = new Thread(me.new Receiver());
		rec.start();
		
		Thread updater = new Thread(me.new Updater());
		updater.start();
		
		// While we receive input from the user 
		while(actions.hasNext()) {
			String command = actions.nextLine();
			
			/* Implementation of various commands */
			if(command.equalsIgnoreCase("close")) {
				
				rec.interrupt();	//Interrupt the receiver thread
				updater.interrupt();	//Interrupt the updater thread
				
				me.timeoutTimer.cancel();
				
				//Close both read and write sockets
				me.closeReadSock();
				break;
			} else if(command.equalsIgnoreCase("showrt")) {
				me.printRoutingTable();
				System.out.println();
			} else if(command.toLowerCase().startsWith("linkdown")) {
				String[] split = command.split(" ");

				try {
					tempIP = split[1];
					tempPort = Integer.parseInt(split[2]);
					int response = me.linkDown(tempIP, tempPort);
					if(response < 0) {
						System.out.println("Command failed.");
					}
				} catch (ArrayIndexOutOfBoundsException a) {
					System.out.println("Bad command. Please try again.");
				}
				System.out.println();
			} else if(command.toLowerCase().startsWith("linkup")) {
				//Implement linkup
				
				try {
					String[] split = command.split(" ");
					tempIP = split[1];
					tempPort = Integer.parseInt(split[2]);
					tempCost = Double.parseDouble(split[3]);
					
					int response = me.linkUP(tempIP, tempPort, tempCost);
					if(response < 0) {
						System.out.println("Command failed.");
					}
				} catch (ArrayIndexOutOfBoundsException a) {
					System.out.println("Bad command. Please try again");
				}
				System.out.println();
			} else if(command.toLowerCase().startsWith("transfer")) {
				if(!me.fileSendingClient) {
					System.out.println("Can't send files.");
					continue;
				}
				
				try {
					//Implement transfer
					String[] split = command.split(" ");
					tempIP = split[1];
					tempPort = Integer.parseInt(split[2]);
					
					DistanceVector dv = me.routingTable.getMap().get(tempIP + ":" + tempPort);
					if(dv==null) {
						System.out.println("Client doesn't exist in routing table");
						continue;
					}
					
					
					String nextHopIP = dv.nextHop().getIP();
					int nextHopPort = dv.nextHop().getPort();

					me.fileMessage = new FileTransferMessage(me.chunkFile, tempIP, tempPort, fileSeqNum);
					me.fileMessage.setSource(me.myIP, me.myPort);
					
					//This person isn't our neighbor so we send to the next hop
					if(!(nextHopIP.equals(me.myIP) && nextHopPort==me.myPort)) {
						me.transferConfig(nextHopIP, nextHopPort);	//Thats who we have to send to.
						me.fileMessage.setNextHop(nextHopIP, nextHopPort);
					} else {
						me.transferConfig(tempIP, tempPort);
						me.fileMessage.setNextHop(tempIP, tempPort);
					}
					
					try {
						me.readFile(); 
					} catch (IOException e) {
						System.out.println("File Problems.");
						continue;
					}
					System.out.println("File Transferring.");
				} catch (ArrayIndexOutOfBoundsException a) {
					// TODO Auto-generated catch block
					System.out.println("Bad command. Try again.");
				}
				System.out.println();
			} else if(command.equals("")){
			} else {
				System.out.println("Command not supported. Try again");
			}
		}
		System.out.println("Client Ending...");
	}
}