/**
 * Distance Vector class that holds the IP/host of a client as well as the cost to it
 * @author Vanshil
 *
 */
public class DistanceVector implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Client destination;
	private double cost;
	private Client nextHop;
	private transient boolean isNeighbor = false;
	
	public DistanceVector(String ip, int port, double cost) {
		destination = new Client(ip, port);
		this.cost = cost;
	}
	
	public Client getDestination() {
		return destination;
	}
	
	public double getCost() {
		return cost;
	}
	
	public Client nextHop() {
		return nextHop;
	}
	
	public void setHop(String nextHopIP, int nextHopPort) {
		nextHop = new Client(nextHopIP, nextHopPort);
	}
	
	public void setCost(double newCost) {
		cost = newCost;
	}
	
	public String toString() {
		return "Destination = " + destination.toString() 
				+ ", Cost = " + cost + ", Link = (" + nextHop + ")"; 
	}
	
	public boolean equals(String IP, int port) {
		if(this.destination.IP.equals(IP) && this.destination.port == port)
			return true;
		return false;
	}
	
	public boolean isNeighbor() {
		return isNeighbor;
	}

	public void setNeighbor(boolean isNeighbor) {
		this.isNeighbor = isNeighbor;
	}

	public class Client implements java.io.Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String IP;
		private int port;
		
		Client(String IP, int port) {
			this.IP = IP;
			this.port = port;
		}
		
		int getPort() {
			return port;
		}
		
		String getIP() {
			return IP;
		}
		
		public String toString() {
			return IP + ":" + port;
		}
		
		public boolean equals(String input) {
			boolean same = false;
			if((IP+":"+port).equals(input)) {
				same = true;
			}
			return same;
		}
	}
	
	
}
