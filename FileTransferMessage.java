import java.util.Date;


public class FileTransferMessage implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Date timestamp;
	String destinationIP;
	int destinationPort;
	byte[] data;
	int fileSequenceNumber;
	String[] hops;
	String sourceIP;
	int sourcePort;
	String nextHopIP;
	int nextHopPort;
	int index;
	String fileName;
	
	public FileTransferMessage(String fileName, String destIP, int destPort, int fileSeqNum) {
		this.fileName = fileName;
		timestamp = new Date();
		destinationIP = destIP;
		destinationPort = destPort;
		fileSequenceNumber = fileSeqNum;
		hops = new String[20];
		index = 0;
	}
	
	public void setNextHop(String IP, int port) {
		this.nextHopIP = IP;
		this.nextHopPort = port;
		hops[index] = IP + ":" + port;
		index++;
	}
	
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public void setSource(String IP, int port) {
		this.sourceIP = IP;
		this.sourcePort = port;
	}
}
