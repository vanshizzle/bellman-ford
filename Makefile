JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	BFClient.java \
	DistanceVector.java \
	RoutingTable.java \
	Tester.java \
	NetworkMessage.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class output
