import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 * My routing table class
 * Contains a list of all the IP/port tuples connected to it and the costs to them
 * @author Vanshil
 *
 */
public class RoutingTable implements java.io.Serializable{
	/**
	 * Some serialization variable
	 */
	private transient static final long serialVersionUID = 1L;
	private Map<String, DistanceVector> map;
	
	/* Used to remember original neighbor costs */
	private transient Map<String, DistanceVector> originalNeighborMap;	//Don't need to serialize this so transient
	private transient boolean dirty;	//Don't need to serialize this so transient
	private int myPort;
	
	public RoutingTable(int port) {
		myPort = port;
		map = new Hashtable<String, DistanceVector>();
		originalNeighborMap = new Hashtable<String, DistanceVector>();
		dirty = false;
	}
	
	//public synchronized void 
	
	public synchronized void addDv(String IP, int port, double cost, String nextHopIP, int nextHopPort, boolean neighbor) {
		DistanceVector d = new DistanceVector(IP, port, cost);
		d.setHop(nextHopIP, nextHopPort);
		d.setNeighbor(neighbor);
		map.put(IP + ":" + port, d);
		if(neighbor) {
			DistanceVector o = new DistanceVector(IP, port, cost);
			o.setHop(IP, port);
			originalNeighborMap.put(IP + ":" + port, o);
		}
	}
	
	public synchronized void printMap() {
		for (String key : map.keySet()) {
			System.out.println(map.get(key).toString());
		}
	}
	
	public synchronized void markDirty() {
		dirty = true;
	}
	
	public synchronized void markClean() {
		dirty = false;
	}
	
	public synchronized boolean hasChanged() {
		return dirty;
	}
	
	public synchronized Map<String, DistanceVector> getMap() {
		return map;
	}

	public int getMyPort() {
		return myPort;
	}
	
	public synchronized Map<String, DistanceVector> getOriginalMap() {
		return originalNeighborMap;
	}
	
	/*
	 * Make a copy of myself 
	 */
	public RoutingTable copy() {
        RoutingTable obj = null;
        try {
            // Write the object out to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(this);	//Uses myself to make a clone
            out.flush();
            out.close();

            // Make an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(
                new ByteArrayInputStream(bos.toByteArray()));
            obj = (RoutingTable) in.readObject();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
        return obj;
	}
}
