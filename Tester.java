import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Just a class used to test some of the code that I write for the main program. 
 * NOT USED IN ACTUALITY FOR THE PROGRAM
 * @author vanshil
 *
 */
public class Tester {
	
	private final static int MESSAGE_TYPES[] = {
			0,	//Route update
			1,	//linkdown
			2,	//linkup
			3
	};

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
//		DatagramSocket sock = new DatagramSocket(5000);
//		byte[] incomingData = new byte[1024];
//		DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
//        sock.receive(incomingPacket);
//        byte[] data = incomingPacket.getData();
//        ByteArrayInputStream in = new ByteArrayInputStream(data);
//        ObjectInputStream is = new ObjectInputStream(in);
//        try {
//        	DistanceVector vec = (DistanceVector) is.readObject();
//        	System.out.println(vec.toString());
//        } catch (ClassNotFoundException c) {
//        	c.printStackTrace();
//        }
//        System.out.println("Dunzo");
//        
//		DatagramSocket writeSock;
//		ByteArrayOutputStream bos;
//		ObjectOutputStream outStream;
//		
//		DistanceVector vec = new DistanceVector("111.111.111.111", 5555, 10.5);
//        String a = "/192.168.1.1";
//        System.out.println(a.replaceFirst("/", ""));
//        String b = "192.168.1.1";
//        System.out.println(b.replaceFirst("/", ""));
//		try{
//			writeSock = new DatagramSocket();
//			bos = new ByteArrayOutputStream(2048);
//			outStream = new ObjectOutputStream(bos);
//			try {
//				outStream.writeObject(vec);
//				DatagramPacket sendPacket = new DatagramPacket(bos.toByteArray(), 
//																bos.size(), 
//																InetAddress.getByName("localhost"),
//																5000);
//				System.out.println(sendPacket.getAddress().toString().replaceFirst("/", ""));
//				writeSock.send(sendPacket);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}	
		DatagramSocket d = new DatagramSocket();
		System.out.println(d.getLocalPort());
		System.out.println(d.getPort());
		
		System.out.println(InetAddress.getLocalHost().getHostAddress());
	}
}
